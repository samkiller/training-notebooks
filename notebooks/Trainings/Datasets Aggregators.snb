{
  "metadata" : {
    "id" : "325f845d-fde0-45df-b964-39e353f1d49d",
    "name" : "Datasets Aggregators",
    "user_save_timestamp" : "1970-01-01T01:00:00.000Z",
    "auto_save_timestamp" : "1970-01-01T01:00:00.000Z",
    "language_info" : {
      "name" : "scala",
      "file_extension" : "scala",
      "codemirror_mode" : "text/x-scala"
    },
    "trusted" : true,
    "sparkNotebook" : null,
    "customLocalRepo" : null,
    "customRepos" : null,
    "customDeps" : null,
    "customImports" : null,
    "customArgs" : null,
    "customSparkConf" : null,
    "customVars" : null
  },
  "cells" : [ {
    "metadata" : {
      "id" : "8FD3E249ED18462A8A3C25E9CCEBBEB1"
    },
    "cell_type" : "markdown",
    "source" : "### Aggregators provide a mechanism for adding up all of the elements in a DataSet (or in each group of a GroupedDataset), returning a single result. \n\n### An Aggregator is similar to a UDAF, but the interface is expressed in terms of JVM objects instead of as a Row. \n\n### Any class that extends Aggregator[A, B, C] can be used, where:\n#### A - specifies the input type to the aggregator\n#### B - specifies the intermediate type durring aggregation\n#### C - specifies the final type output by the aggregation\n\n### Before using an Aggregator in a Dataset operation, you must call toColumn, passing any encoders that can't be infered automatically by the compiler.\n"
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "EE96183D16464FCD83CDCB4C5BBFDD60"
    },
    "cell_type" : "code",
    "source" : "import org.apache.spark.sql.expressions.MutableAggregationBuffer\nimport org.apache.spark.sql.expressions.UserDefinedAggregateFunction\nimport org.apache.spark.sql.Row\nimport org.apache.spark.sql.types._\nimport org.apache.spark.sql.types.{StructType, StructField, DataType, LongType, DoubleType}\n",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "import org.apache.spark.sql.expressions.MutableAggregationBuffer\nimport org.apache.spark.sql.expressions.UserDefinedAggregateFunction\nimport org.apache.spark.sql.Row\nimport org.apache.spark.sql.types._\nimport org.apache.spark.sql.types.{StructType, StructField, DataType, LongType, DoubleType}\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : ""
      },
      "output_type" : "execute_result",
      "execution_count" : 19,
      "time" : "Took: 0.646s, at 2017-06-25 23:34"
    } ]
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "61C18AE5746F45C28CBC76EE346D4E90"
    },
    "cell_type" : "code",
    "source" : "class GeometricMean extends UserDefinedAggregateFunction {\n  // This is the input fields for your aggregate function.\n  override def inputSchema: org.apache.spark.sql.types.StructType =\n    StructType(StructField(\"value\", DoubleType) :: Nil)\n\n  // This is the internal fields you keep for computing your aggregate.\n  override def bufferSchema: StructType = StructType(\n    StructField(\"count\", LongType) ::\n    StructField(\"product\", DoubleType) :: Nil\n  )\n\n  // This is the output type of your aggregatation function.\n  override def dataType: DataType = DoubleType\n\n  override def deterministic: Boolean = true\n\n  // This is the initial value for your buffer schema.\n  override def initialize(buffer: MutableAggregationBuffer): Unit = {\n    buffer(0) = 0L\n    buffer(1) = 1.0\n  }\n\n  // This is how to update your buffer schema given an input.\n  override def update(buffer: MutableAggregationBuffer, input: Row): Unit = {\n    buffer(0) = buffer.getAs[Long](0) + 1\n    buffer(1) = buffer.getAs[Double](1) * input.getAs[Double](0)\n  }\n\n  // This is how to merge two objects with the bufferSchema type.\n  override def merge(buffer1: MutableAggregationBuffer, buffer2: Row): Unit = {\n    buffer1(0) = buffer1.getAs[Long](0) + buffer2.getAs[Long](0)\n    buffer1(1) = buffer1.getAs[Double](1) * buffer2.getAs[Double](1)\n  }\n\n  // This is where you output the final value, given the final value of your bufferSchema.\n  override def evaluate(buffer: Row): Any = {\n    math.pow(buffer.getDouble(1), 1.toDouble / buffer.getLong(0))\n  }\n}",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "defined class GeometricMean\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : ""
      },
      "output_type" : "execute_result",
      "execution_count" : 20,
      "time" : "Took: 0.482s, at 2017-06-25 23:34"
    } ]
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "EBB4AA2DEE384EB1809DFC29A2D19E40"
    },
    "cell_type" : "code",
    "source" : "sparkSession.udf.register(\"gm\", new GeometricMean)",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "res25: org.apache.spark.sql.expressions.UserDefinedAggregateFunction = $iwC$$iwC$GeometricMean@6093334c\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : "$line88.$read$$iwC$$iwC$GeometricMean@6093334c"
      },
      "output_type" : "execute_result",
      "execution_count" : 22,
      "time" : "Took: 0.600s, at 2017-06-25 23:34"
    } ]
  }, {
    "metadata" : {
      "id" : "F9BDEF28E3AD4AAB8B946D40E61F293A"
    },
    "cell_type" : "markdown",
    "source" : "Let's use it"
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "2D69A8A2B9CF47FA80282EF3199FF5C6"
    },
    "cell_type" : "code",
    "source" : "import org.apache.spark.sql.functions._\n\nval ids = sparkSession.range(1, 100)\nids.createOrReplaceTempView (\"ids\")\n\nval df = sparkSession.sql(\"select id, id % 3 as group_id from ids\")\ndf.createOrReplaceTempView (\"simple\")",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "import org.apache.spark.sql.functions._\nids: org.apache.spark.sql.Dataset[Long] = [id: bigint]\ndf: org.apache.spark.sql.DataFrame = [id: bigint, group_id: bigint]\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : ""
      },
      "output_type" : "execute_result",
      "execution_count" : 29,
      "time" : "Took: 1.507s, at 2017-06-25 23:37"
    } ]
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "DFEF6A17AA77456FA97313B18D4ED0D5"
    },
    "cell_type" : "code",
    "source" : "sparkSession.sql( \"\"\" select group_id, gm(id) from simple group by group_id \"\"\")\n",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "res43: org.apache.spark.sql.DataFrame = [group_id: bigint, geometricmean(CAST(id AS DOUBLE)): double]\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : "<div class=\"df-canvas\">\n      <script data-this=\"{&quot;dataId&quot;:&quot;anone41a7d10283c6f64e945216f02d43157&quot;,&quot;partitionIndexId&quot;:&quot;anon845bd1eecb3b4b00a788a33a71c038a1&quot;,&quot;numPartitions&quot;:1,&quot;dfSchema&quot;:{&quot;type&quot;:&quot;struct&quot;,&quot;fields&quot;:[{&quot;name&quot;:&quot;group_id&quot;,&quot;type&quot;:&quot;long&quot;,&quot;nullable&quot;:true,&quot;metadata&quot;:{}},{&quot;name&quot;:&quot;geometricmean(CAST(id AS DOUBLE))&quot;,&quot;type&quot;:&quot;double&quot;,&quot;nullable&quot;:true,&quot;metadata&quot;:{}}]}}\" type=\"text/x-scoped-javascript\">/*<![CDATA[*/req(['../javascripts/notebook/dataframe','../javascripts/notebook/consoleDir'], \n      function(dataframe, extension) {\n        dataframe.call(data, this, extension);\n      }\n    );/*]]>*/</script>\n      <link rel=\"stylesheet\" href=\"/assets/stylesheets/ipython/css/dataframe.css\" type=\"text/css\"/>\n    </div>"
      },
      "output_type" : "execute_result",
      "execution_count" : 31,
      "time" : "Took: 1.464s, at 2017-06-25 23:38"
    } ]
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "E47F39A71B8347258617E7CF41C96408"
    },
    "cell_type" : "code",
    "source" : "val gm = new GeometricMean\n\n// Show the geometric mean of values of column \"id\".\ndf.groupBy(\"group_id\").agg(gm(col(\"id\")).as(\"GeometricMean\")).show()\n",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "+--------+-----------------+\n|group_id|    GeometricMean|\n+--------+-----------------+\n|       0|39.48893002549846|\n|       1|35.70963117857145|\n|       2|37.76617628401597|\n+--------+-----------------+\n\ngm: GeometricMean = $iwC$$iwC$GeometricMean@324bb3f8\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : ""
      },
      "output_type" : "execute_result",
      "execution_count" : 32,
      "time" : "Took: 1.347s, at 2017-06-25 23:38"
    } ]
  }, {
    "metadata" : {
      "trusted" : true,
      "input_collapsed" : false,
      "collapsed" : false,
      "id" : "5A17BC0C7E7340CB9B5B493955BE841E"
    },
    "cell_type" : "code",
    "source" : "// Invoke the UDAF by its assigned name.\ndf.groupBy(\"group_id\").agg(expr(\"gm(id) as GeometricMean\")).show",
    "outputs" : [ {
      "name" : "stdout",
      "output_type" : "stream",
      "text" : "+--------+-----------------+\n|group_id|    GeometricMean|\n+--------+-----------------+\n|       0|39.48893002549846|\n|       1|35.70963117857145|\n|       2|37.76617628401597|\n+--------+-----------------+\n\n"
    }, {
      "metadata" : { },
      "data" : {
        "text/html" : ""
      },
      "output_type" : "execute_result",
      "execution_count" : 33,
      "time" : "Took: 1.231s, at 2017-06-25 23:38"
    } ]
  } ],
  "nbformat" : 4
}