FROM ubuntu:latest
MAINTAINER Sam BESSALAH samklr@me.com

ENV SNB_BINARY https://s3-eu-west-1.amazonaws.com/sk2/bins/spark-notebook-0.8.0-SNAPSHOT-scala-2.10.6-spark-2.1.1-hadoop-2.7.3-with-hive.zip
ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

RUN \
  apt-get update && \
  apt-get install -y software-properties-common unzip htop


# Install Java.
RUN \
  echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
  add-apt-repository -y ppa:webupd8team/java && \
  apt-get update && \
  apt-get install -y oracle-java8-installer && \
  rm -rf /var/lib/apt/lists/* && \
  rm -rf /var/cache/oracle-jdk8-installer


ENV JAVA_HOME /usr/lib/jvm/java-8-oracle

RUN wget https://s3-eu-west-1.amazonaws.com/sk2/bins/spark-notebook-0.8.0-SNAPSHOT-scala-2.10.6-spark-2.1.1-hadoop-2.7.3-with-hive.zip

RUN unzip spark-notebook-0.8.0-SNAPSHOT-scala-2.10.6-spark-2.1.1-hadoop-2.7.3-with-hive.zip
RUN mv  spark-notebook-0.8.0-SNAPSHOT-scala-2.10.6-spark-2.1.1-hadoop-2.7.3-with-hive spark-notebook
