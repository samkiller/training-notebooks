#!/bin/sh
sbt -D"spark.version"="2.1.1" -D"hadoop.version"="2.7.3" -D"with.hive=true" -D"with.parquet=true" -D"jets3t.version"="0.9.3" -Dmesos.version="1.3.0" clean dist
